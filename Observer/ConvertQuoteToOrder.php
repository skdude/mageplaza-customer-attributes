<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomerAttributes
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomerAttributes\Observer;

use Exception;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Model\AbstractExtensibleModel;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\Quote\Model\Quote;
use Magento\Sales\Api\Data\OrderInterface;
use Mageplaza\CustomerAttributes\Helper\Data;
use Zend_Validate_Exception;
use Zend_Validate_File_Upload;

/**
 * Class ConvertQuoteToOrder
 * @package Mageplaza\CustomerAttributes\Observer
 */
class ConvertQuoteToOrder implements ObserverInterface
{
    /**
     * @var string
     */
    protected $scope = 'order';

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var Zend_Validate_File_Upload
     */
    private $fileUpload;

    /**
     * @var AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ConvertQuoteToOrder constructor.
     *
     * @param Data $helperData
     * @param Zend_Validate_File_Upload $fileUpload
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param Filesystem $filesystem
     */
    public function __construct(
        Data $helperData,
        Zend_Validate_File_Upload $fileUpload,
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        Filesystem $filesystem
    ) {
        $this->helperData                    = $helperData;
        $this->fileUpload                    = $fileUpload;
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->filesystem                    = $filesystem;
    }

    /**
     * @param Observer $observer
     *
     * @return $this|void
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        /** @var AbstractExtensibleModel|OrderInterface $order */
        $order = $observer->getEvent()->getOrder();
        /** @var Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        if (!empty($this->fileUpload->getFiles()[$this->scope])) {
            foreach ($this->formatFilesArray() as $index => $file) {
                // skip case when no file is chosen
                if (empty($file['tmp_name'])) {
                    continue;
                }

                /** @var Uploader $uploader */
                $uploader = $this->helperData->createObject(Uploader::class, ['fileId' => $file]);

                $attribute = $this->attributeMetadataDataProvider->getAttribute('customer', $index);
                if ($attribute->getFrontendInput() === 'image') {
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                }
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);

                $directoryRead = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);

                $result = $this->helperData->moveTemporaryFile(
                    $uploader->save($directoryRead->getAbsolutePath(Data::TEMPLATE_CUSTOMER_MEDIA_PATH)),
                    Data::TEMPLATE_CUSTOMER_MEDIA_PATH
                );

                $quote->setData('customer_' . $index, $result);
            }
        }

        $attributes = $this->helperData->getUserDefinedAttributeCodes('customer');
        foreach ($attributes as $attribute) {
            $order->setData('customer_' . $attribute, $quote->getData('customer_' . $attribute));
        }

        return $this;
    }

    /**
     * Format files array for multiple uploading files
     *
     * @return array
     * @throws Zend_Validate_Exception
     */
    protected function formatFilesArray()
    {
        $files = [];

        foreach ((array) $this->fileUpload->getFiles()[$this->scope] as $key => $value) {
            foreach ((array) $value as $index => $item) {
                if (!is_array($item)) {
                    $item = [$item];
                }

                foreach ($item as $a => $b) {
                    $files[$a][$key] = $b;
                }
            }
        }

        return $files;
    }
}

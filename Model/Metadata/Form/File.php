<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomerAttributes
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomerAttributes\Model\Metadata\Form;

use Magento\Backend\Model\UrlInterface;
use Magento\Customer\Api\Data\AttributeMetadataInterface;
use Magento\Customer\Api\Data\ValidationRuleInterface;
use Magento\Customer\Model\FileProcessorFactory;
use Magento\Customer\Model\Metadata\ElementFactory;
use Magento\Eav\Model\AttributeDataFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Url\EncoderInterface;
use Magento\MediaStorage\Model\File\Validator\NotProtectedExtension;
use Mageplaza\CustomerAttributes\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * Class File
 * @package Mageplaza\CustomerAttributes\Model\Metadata\Form
 */
class File extends \Magento\Customer\Model\Metadata\Form\File
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var UrlInterface
     */
    protected $backendUrl;

    /**
     * File constructor.
     *
     * @param TimezoneInterface $localeDate
     * @param LoggerInterface $logger
     * @param AttributeMetadataInterface $attribute
     * @param ResolverInterface $localeResolver
     * @param string|array $value
     * @param string $entityTypeCode
     * @param bool $isAjax
     * @param EncoderInterface $urlEncoder
     * @param NotProtectedExtension $fileValidator
     * @param Filesystem $fileSystem
     * @param UploaderFactory $uploaderFactory
     * @param ManagerInterface $messageManager
     * @param UrlInterface $backendUrl
     * @param FileProcessorFactory|null $fileProcessorFactory
     */
    public function __construct(
        TimezoneInterface $localeDate,
        LoggerInterface $logger,
        AttributeMetadataInterface $attribute,
        ResolverInterface $localeResolver,
        $value,
        $entityTypeCode,
        $isAjax,
        EncoderInterface $urlEncoder,
        NotProtectedExtension $fileValidator,
        Filesystem $fileSystem,
        UploaderFactory $uploaderFactory,
        ManagerInterface $messageManager,
        UrlInterface $backendUrl,
        FileProcessorFactory $fileProcessorFactory = null
    ) {
        $this->messageManager = $messageManager;
        $this->backendUrl     = $backendUrl;

        if (is_array($value) && isset($value['value'])) {
            $value = $value['value'];
        }

        /** @var Data $helper */
        $helper = ObjectManager::getInstance()->get(Data::class);

        if ($helper->versionCompare('2.2.0')) {
            parent::__construct(
                $localeDate,
                $logger,
                $attribute,
                $localeResolver,
                $value,
                $entityTypeCode,
                $isAjax,
                $urlEncoder,
                $fileValidator,
                $fileSystem,
                $uploaderFactory
            );
        } else {
            parent::__construct(
                $localeDate,
                $logger,
                $attribute,
                $localeResolver,
                $value,
                $entityTypeCode,
                $isAjax,
                $urlEncoder,
                $fileValidator,
                $fileSystem,
                $uploaderFactory,
                $fileProcessorFactory
            );
        }
    }

    /**
     * @param string $format
     *
     * @return array|string
     */
    public function outputValue($format = ElementFactory::OUTPUT_FORMAT_TEXT)
    {
        if ($format === AttributeDataFactory::OUTPUT_FORMAT_HTML) {
            return $this->_value;
        }

        return parent::outputValue($format);
    }

    /**
     * @param array|string $value
     *
     * @return bool|int|string
     * @throws LocalizedException
     */
    protected function processInputFieldValue($value)
    {
        $extension         = $this->getFileExtension($value);
        $allowedExtensions = $this->getAllowedExtensions($this->getAttribute()->getValidationRules());

        if ($extension && !$this->checkAllowedExtension($extension, $allowedExtensions)) {
            $this->messageManager->addErrorMessage(__('Disallowed file type. The image cannot be saved.'));

            return $this->_value;
        }

        return parent::processInputFieldValue($value);
    }

    /**
     * @param array $file
     *
     * @return string
     */
    private function getFileExtension($file)
    {
        return !empty($file['name']) ? pathinfo($file['name'], PATHINFO_EXTENSION) : null;
    }

    /**
     * @param string $extension
     * @param array $allowedExtensions
     *
     * @return bool
     */
    private function checkAllowedExtension($extension, $allowedExtensions)
    {
        if (!is_array($allowedExtensions) || empty($allowedExtensions)) {
            return true;
        }

        return in_array(strtolower($extension), $allowedExtensions, true);
    }

    /**
     * @param ValidationRuleInterface[] $validationRules
     *
     * @return array
     */
    private function getAllowedExtensions($validationRules)
    {
        $allowedExtensions = [];

        foreach ($validationRules as $validationRule) {
            if ($validationRule->getName() === 'file_extensions') {
                $allowedExtensions = explode(',', $validationRule->getValue());
                array_walk($allowedExtensions, function (&$value) {
                    $value = strtolower(trim($value));
                });
                break;
            }
        }

        return $allowedExtensions;
    }
}

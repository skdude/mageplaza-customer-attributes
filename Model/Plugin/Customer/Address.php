<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomerAttributes\Model\Plugin\Customer;

/**
 * Class Address
 * @package Mageplaza\CustomerAttributes\Model\Plugin\Customer
 */
class Address
{
    /**
     * @param \Magento\Customer\Model\Address $subject
     * @param \Magento\Customer\Model\Address $result
     *
     * @return \Magento\Customer\Model\Address
     */
    public function afterUpdateData(\Magento\Customer\Model\Address $subject, $result)
    {
        $result->setShouldIgnoreValidation(true);

        return $result;
    }
}
